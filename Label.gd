extends Label

func _ready():
	rect_scale = Vector2(1.5/get_parent().scale[0], 1.5/get_parent().scale[1])
	align = Label.ALIGN_CENTER
	valign = Label.VALIGN_CENTER

func _process(delta):
	text = str(round(get_parent().population))
	rect_position = - rect_size * rect_scale / 2.0