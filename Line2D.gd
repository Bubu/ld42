extends Line2D

export(NodePath) var fromPath
export(NodePath) var toPath
var from
var to

func _ready():
	if fromPath:
		from = get_node(fromPath)
	if toPath:
		to = get_node(toPath)
	if from and to:
		setup(from, to)
	
func setup(fromarg, toarg):
	from = fromarg
	to = toarg
	set_point_position(0,
		Vector2(from.position + 
			(to.position - from.position).normalized() * (from.texture.get_size()[0] * from.scale[0] / 2.0 )))
	set_point_position(1,
		Vector2(to.position + 
			(from.position - to.position).normalized() * (to.texture.get_size()[0] * to.scale[0] / 2.0 )))

func _on_Button_pressed():
	from.connections.remove(from.connections.find(to))
	queue_free() # replace with function body