extends Sprite
var buttonScene = preload("res://Button.tscn")

func _ready():
	var line = get_parent()
	position = Vector2((line.get_point_position(0).x + line.get_point_position(1).x) / 2.0
		, (line.get_point_position(0).y + line.get_point_position(1).y) / 2.0)



func _input(event):
		if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
			if inside(event.position):
				get_parent()._on_Button_pressed()
				
func inside(point):
	return point.distance_to(global_position) < texture.get_size()[0] * scale[0] / 2.0 