extends Sprite
var default_texture = preload("res://planet1.png")
var clicked_texture = preload("res://planet2.png")
var lineScene = preload("res://Line2D.tscn")
var is_pressed = false
const MIGRATION = 0.05
var connections = []

export var population = 200
export var max_population = 800
var extinction = 0

onready var world = get_node("/root/world")

func _ready():
	pass

func _process(delta):
	material.set_shader_param("fill", population/max_population)

func _physics_process(delta):
	population = population * 1.00005
	for con in connections:
			population = population - MIGRATION
			con.population = con.population + MIGRATION
	if population > max_population:
		extinction = 600
	if extinction > 0:
		population = population - population * 0.002
		extinction -=1

func _input(event):
	if event is InputEventMouseMotion and world.dragging_from:
		if inside(event.position):
			set_texture(clicked_texture)
			get_tree().set_input_as_handled()
		elif not is_pressed:
			set_texture(default_texture)
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		if inside(event.position):
			world.dragging_from = self
			is_pressed = true
			set_texture(clicked_texture)
			get_tree().set_input_as_handled()
	if event is InputEventMouseButton and not event.pressed and event.button_index == BUTTON_LEFT:
		if inside(event.position):
			if world.dragging_from != self:
				world.dragging_from.connections.append(self)
				var line = lineScene.instance()
				line.setup(world.dragging_from, self)
				get_node("/root/world/Lines").add_child(line)
				set_texture(default_texture)
				is_pressed = false
				world.dragging_from = null
		set_texture(default_texture)
		is_pressed = false

func inside(point):
	return point.distance_to(position) < texture.get_size()[0] * scale[0] / 2.0 