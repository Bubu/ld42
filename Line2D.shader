shader_type canvas_item;
render_mode unshaded;

uniform float speed : hint_range(-5, 5);

void fragment(){
	COLOR = texture(TEXTURE,UV + vec2(speed * TIME));
}